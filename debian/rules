#!/usr/bin/make -f
# -*- makefile -*-

# Disable ceph dependent packages for architectures without ceph
# This list should match the Build-Depends on ceph libraries in debian/control
#
# Ceph requires boost-context and boost-coroutine
# These are only available on these Debian architectures:
#   i386 hurd-i386 kfreebsd-i386 amd64 kfreebsd-amd64 armel armhf arm64
#   mips mipsel mips64el powerpc ppc64el riscv64 s390x
#
# Ceph requires Java
# Java is not available on these Debian architectures:
#   hppa hurd-i386 kfreebsd-i386 kfreebsd-amd64
ifneq ($(filter $(DEB_HOST_ARCH),i386 amd64 armel armhf arm64 mips mipsel mips64el powerpc ppc64el riscv64 s390x),)
CEPH = ON
else
CEPH = OFF
endif

BUILD_DOC = $(if $(filter xrootd-doc,$(shell dh_listpackages)),TRUE,FALSE)

%:
	dh $@ --with python3

override_dh_auto_configure:
	dh_auto_configure -- \
	    -DXRDCEPH_SUBMODULE:BOOL=$(CEPH) \
	    -DENABLE_XRDCLHTTP:BOOL=ON \
	    -DPIP_OPTIONS="--no-deps --disable-pip-version-check --verbose"

override_dh_auto_build:
	dh_auto_build
	[ "$(BUILD_DOC)" = "FALSE" ] || doxygen Doxyfile

override_dh_auto_clean:
	dh_auto_clean
	rm -rf doxydoc
	rm -rf bindings/python/docs/build

override_dh_auto_install:
	DEB_PYTHON_INSTALL_LAYOUT=deb dh_auto_install

	rm -f debian/tmp/usr/lib/*/libXrdCephPosix.so

	rm -f debian/tmp/usr/lib/python3/dist-packages/xrootd-*.*-info/direct_url.json
	rm -f debian/tmp/usr/lib/python3/dist-packages/xrootd-*.*-info/RECORD
	[ -r debian/tmp/usr/lib/python3/dist-packages/xrootd-*.*-info/INSTALLER ] && \
	    sed s/pip/dpkg/ \
	    -i debian/tmp/usr/lib/python3/dist-packages/xrootd-*.*-info/INSTALLER

	[ "$(BUILD_DOC)" = "FALSE" ] || ( \
	LD_LIBRARY_PATH=$${LD_LIBRARY_PATH}:$(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH) \
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages \
	PYTHONDONTWRITEBYTECODE=1 \
	make -C bindings/python/docs html && \
	mv bindings/python/docs/build/html bindings/python/docs/build/python )

	# Service unit files
	mkdir -p debian/tmp/usr/lib/systemd/system
	install -m 644 packaging/common/xrootd@.service debian/tmp/usr/lib/systemd/system
	install -m 644 packaging/common/xrootd@.socket debian/tmp/usr/lib/systemd/system
	install -m 644 packaging/common/xrdhttp@.socket debian/tmp/usr/lib/systemd/system
	install -m 644 packaging/common/cmsd@.service debian/tmp/usr/lib/systemd/system
	install -m 644 packaging/common/frm_xfrd@.service debian/tmp/usr/lib/systemd/system
	install -m 644 packaging/common/frm_purged@.service debian/tmp/usr/lib/systemd/system
	mkdir -p debian/tmp/usr/lib/tmpfiles.d
	install -m 644 packaging/rhel/xrootd.tmpfiles debian/tmp/usr/lib/tmpfiles.d/xrootd.conf

	# Server config
	mkdir -p debian/tmp/etc/xrootd
	install -m 644 -p packaging/common/xrootd-clustered.cfg \
	    debian/tmp/etc/xrootd/xrootd-clustered.cfg
	install -m 644 -p packaging/common/xrootd-standalone.cfg \
	    debian/tmp/etc/xrootd/xrootd-standalone.cfg
	install -m 644 -p packaging/common/xrootd-filecache-clustered.cfg \
	    debian/tmp/etc/xrootd/xrootd-filecache-clustered.cfg
	install -m 644 -p packaging/common/xrootd-filecache-standalone.cfg \
	    debian/tmp/etc/xrootd/xrootd-filecache-standalone.cfg
	sed 's!/usr/lib64/!!' packaging/common/xrootd-http.cfg > \
	    debian/tmp/etc/xrootd/xrootd-http.cfg

	# Client config
	mkdir -p debian/tmp/etc/xrootd/client.plugins.d
	install -m 644 -p packaging/common/client.conf \
	    debian/tmp/etc/xrootd/client.conf
	sed 's!/usr/lib/!!' packaging/common/client-plugin.conf.example > \
	    debian/tmp/etc/xrootd/client.plugins.d/client-plugin.conf.example
	sed -e 's!/usr/lib64/!!' -e 's!-5!!' packaging/common/recorder.conf > \
	    debian/tmp/etc/xrootd/client.plugins.d/recorder.conf
	sed 's!/usr/lib64/!!' packaging/common/http.client.conf.example > \
	    debian/tmp/etc/xrootd/client.plugins.d/xrdcl-http-plugin.conf

	chmod 644 debian/tmp/usr/share/xrootd/utils/XrdCmsNotify.pm
	sed 's!/usr/bin/env perl!/usr/bin/perl!' -i \
	    debian/tmp/usr/share/xrootd/utils/netchk \
	    debian/tmp/usr/share/xrootd/utils/XrdCmsNotify.pm \
	    debian/tmp/usr/share/xrootd/utils/XrdOlbMonPerf

	sed 's!/usr/bin/env bash!/bin/bash!' -i \
	    debian/tmp/usr/bin/xrootd-config

	mkdir -p debian/tmp/etc/xrootd/config.d

	mkdir -p debian/tmp/var/log/xrootd
	mkdir -p debian/tmp/var/spool/xrootd

	mkdir -p debian/tmp/etc/logrotate.d
	install -m 644 -p packaging/common/xrootd.logrotate \
	    debian/tmp/etc/logrotate.d/xrootd
